import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule}from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import {HttpClientModule} from '@angular/common/http'
import { AppComponent } from './app.component';
import { HeadComponent } from './head/head.component';
import { HomeComponent } from './head/home/home.component';
import { LoginComponent } from './head/login/login.component';
import { RegisterComponent } from './head/register/register.component';
import { UserdashboardComponent } from './head/userdashboard/userdashboard.component';


@NgModule({
  declarations: [
    AppComponent,
    HeadComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    UserdashboardComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
   
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
