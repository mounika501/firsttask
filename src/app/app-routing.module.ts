import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeadComponent } from './head/head.component';
import { HomeComponent } from './head/home/home.component';
import { LoginComponent } from './head/login/login.component';
import { RegisterComponent } from './head/register/register.component';
import { UserdashboardComponent } from './head/userdashboard/userdashboard.component';

const routes: Routes = [
  {
    path:"",
    redirectTo:"head",
    pathMatch:"full"
  },
  {
    path:'head',
    component:HeadComponent,
    children:[
      {
        path:'home',
        component:HomeComponent
      },
      {
        path:'login',
        component:LoginComponent
      },
      {
        path:'register',
        component:RegisterComponent
      },
      {
        path:'userdashboard',
        component:UserdashboardComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
