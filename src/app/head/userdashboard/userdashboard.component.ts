import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-userdashboard',
  templateUrl: './userdashboard.component.html',
  styleUrls: ['./userdashboard.component.css']
})
export class UserdashboardComponent implements OnInit {
list:any[]
data1:any[]
  constructor(private httpc:HttpClient) { }

  ngOnInit() {
    this.httpc.get('/user/profile').subscribe(res=>
      {
        this.list=res['message'];
        console.log(this.list)


        
       
   
      })
  }


  b:boolean=false;
  objectToModify:object;
  editProfile(obj)
  {
    this.objectToModify=obj;
    console.log(this.objectToModify);
    
    this.b=true;
  }
  onSubmit(val)
  {
    this.b=false;
    this.httpc.put('user/update',val).subscribe(res=>
     {
       alert(res['message']);
        
     })
 
 
    }

    deleteData(emailid)
    {
      // console.log(emailid);
      this.httpc.delete(`/user/delete`,emailid).subscribe(res=>
        {
          alert(res['message']);
          this.data1=res['data'];
        })
    }
  


}
