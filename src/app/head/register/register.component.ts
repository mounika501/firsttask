import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
data:any[];
  constructor(private router:Router,private httpc:HttpClient) { }

  ngOnInit() {
  }

  submitRegisterData(value)
  {
    this.data=value;
    console.log(this.data);

    if(value.firstname=="" || value.middlename=="" || value.lastname=="" || value.emailid =="" || value.number=="")
    {
      alert("please fill all the required fields")
    }
 
     else if(value.pswd==value.cpswd)
    {
    
    this.httpc.post('/user/register',this.data).subscribe(res=>
    {
      if(res['message']=='success')
      {
        alert('registration success')
        this.router.navigate(['head/login']);
      }
    
    })
  }
  else{
    alert("password and confirm password is not matched")
  }
  }

}
