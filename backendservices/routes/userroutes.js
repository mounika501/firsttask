const express=require('express')
const app=express();
//import dbconfig
const initDb=require('../DBConfig').initDb;
const getDb=require('../DBConfig').getDb;

//import userroutes
const userroutes=express.Router();


initDb();
userroutes.post('/register',(req,res)=>
{
   
    if(req.body=={})
    {
        console.log("no data is received by database")
    }
    else
    {
        dbo=getDb();
        dbo.collection('registercollection').insertOne(req.body,(err,success)=>
        {
            if(err)
            {
                console.log(err,"error in inserting the data")
            }
            else{
                res.json({'message':'success'})
            }
        })
    }
})
//get profile

userroutes.get('/profile',(req,res)=>
{
    dbo=getDb();
    dbo.collection("registercollection").find().toArray((err,dataArray)=>
    {
        if(err)
        {
            console.log("error in reading data");
        }
        else if(dataArray==0)
        {
            res.json({"message":"no data found"});
        }
        else
        {
            res.json({"message":dataArray});
        }
    })
});



userroutes.put('/update',(req,res,next)=>
{
    console.log(req.body);
    dbo=getDb();
    dbo.collection('ownercollection').update({emailid:{$eq:req.body.emailid}},{$set:{number:req.body.number,firstname:req.body.firstname,lastname:req.body.lastname,middlename:req.body.middlename,emailid:req.body.emailid}},(err,success)=>
    {
        if(err)
        {
            next(err);

        }
        else
        {
            res.json({message:"update successfully"});
        }
    })
});



userroutes.delete('/delete/:emailid',(req,res)=>{
    console.log(req.params);
    dbo=getDb();
    dbo.collection('registercollection').delete({emailid:{$eq:req.params.emailid}},(err,success)=>
    {
        if(err)
        {
            console.log(err);
            console.log("error in delete operation");
        }
        else
        {
            dbo.collection("registercollection").find().toArray((err,dataArray)=>
            {
                if(err)
                {
                    console.log("error in reading");

                }   
                else
                {
                    res.json({message:"Record deleted",
                                data:dataArray
                            })
                }
            })
        }
    })

})



module.exports=userroutes;