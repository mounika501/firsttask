const mongo=require('mongodb').MongoClient;
const url="mongodb://mouni:mouni@cluster0-shard-00-00-lk0wl.mongodb.net:27017,cluster0-shard-00-01-lk0wl.mongodb.net:27017,cluster0-shard-00-02-lk0wl.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority";

function initDb()
{
    mongo.connect(url,{useNewUrlParser:true},{ useUnifiedTopology: true } ,(err,client)=>
    {
        if(err)
        {
            console.log(err,"error in connecting to database")
        }
        else{
            console.log("database connected")
            dbo=client.db('formdatabase')
        }
    })
}
function getDb()
{
    return dbo;
}
module.exports={
    initDb,
    getDb
}