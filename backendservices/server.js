const exp=require('express');
const app=exp();

const bodyparser=require("body-parser");
app.use(bodyparser.json());


const userroutes=require('./routes/userroutes');

app.use("/user",userroutes);

const path=require("path");
console.log(__dirname);
app.use(exp.static(path.join(__dirname,'../dist/form/')));


const port=3000;
app.listen( port,()=>console.log(`server running on port ${port}`));